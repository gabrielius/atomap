.. _api:

=================
API documentation
=================

Classes
-------

Atom lattice
============
.. autoclass:: atomap.atom_lattice.Atom_Lattice
    :members:
    :undoc-members:

Sublattice
===========
.. autoclass:: atomap.sublattice.Sublattice
    :members:
    :undoc-members:

Atom plane
==========
.. autoclass:: atomap.atom_plane.Atom_Plane
    :members:
    :undoc-members:

Atom position 
=============
.. autoclass:: atomap.atom_position.Atom_Position
    :members:
    :undoc-members:

Process parameters
==================
.. autoclass:: atomap.process_parameters.ModelParametersBase
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.GenericStructure
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.PerovskiteOxide110
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.SrTiO3_110
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.SublatticeParameterBase
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.GenericSublattice
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.PerovskiteOxide110SublatticeACation
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.PerovskiteOxide110SublatticeBCation
    :members:
    :undoc-members:

.. autoclass:: atomap.process_parameters.PerovskiteOxide110SublatticeOxygen
    :members:
    :undoc-members:

Modules
-------

Atom finding refining
=====================
.. automodule:: atomap.atom_finding_refining
    :members:

Input output (IO)
=================
.. automodule:: atomap.io
    :members:

Main
====
.. automodule:: atomap.main
    :members:

Plotting
========
.. automodule:: atomap.plotting
    :members:

Tools
=====
.. automodule:: atomap.tools
    :members:
