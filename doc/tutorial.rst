.. _tutorial:


========
Tutorial
========

Starting Python
---------------

The first step is starting an interactive Python environment (IPython).

Linux
^^^^^

Open a terminal and start `ipython3`:

.. code-block:: bash

    $ ipython3

If `ipython3` is not available, try `ipython`:

.. code-block:: bash

    $ ipython

Windows
^^^^^^^

This depends on the installation method.
If Anaconda was used, there should be an *Anaconda3* folder in the start menu.
Start the interactive Python environment, it should be called either *IPython* or *Jupyter QtConsole*.
This will open a command line prompt.
This prompt will be referred to as the *IPython terminal*.

Getting test data
-----------------

The `>>>` used in the documentation means the comment should be typed inside some kind of Python prompt, so do not include these when actually running the code.
So for the first command below, only `mkdir atomap_testing` should be typed or copied into the IPython terminal.
In the IPython terminal:

.. code-block:: python

    >>> mkdir atomap_testing
    >>> cd atomap_testing

Firstly we need to download some test datasets from the Atomap repository:

.. code-block:: python

    >>> import urllib.request
    >>> urllib.request.urlretrieve("https://gitlab.com/atomap/atomap/raw/master/atomap/tests/datasets/test_ADF_cropped.hdf5", "test_ADF_cropped.hdf5")

This will grab a data file with a High Angle Annular Dark Field image of |SrTiO3| projected along the [110] zone axis.
The file should appear in our current folder:

.. code-block:: python

    >>> ls
    test_ADF_cropped.hdf5

Finding the peak separation
----------------------------

For Atomap to do its analysis the smallest peak separation has to be specified.
Getting this separation is done using the `get_feature_separation` function, which returns a HyperSpy signal:

.. code-block:: python

    >>> import hyperspy.api as hs
    >>> import atomap.api as am
    >>> s = hs.load("test_ADF_cropped.hdf5")
    >>> s_peaks = am.get_feature_separation(s)
    >>> s_peaks.plot(plot_markers=True)

This should open two figures like shown below:

.. image:: images/tutorial/peak_separation_initial.jpg
    :scale: 50 %
    :align: center

The right figure shows where the peak finding function has located a peak, the left
figure shows the minimum feature separation in pixels on the x-axis. Use the left-right arrow keys to change
the minimum feature separation.

The requirements for the peak separation are:
    1. With an optimal peak separation, only atoms from one sublattice should be marked.
    2. In addition, all the atoms from the first sublattice should be marked.

So the peak separation shown in the figure above is not good.

It should look something like this:

.. image:: images/tutorial/peak_separation_good.jpg
    :scale: 50 %
    :align: center

Note, requirement 2 does not extend to the edges, so this would also work:

.. image:: images/tutorial/peak_separation_ok.jpg
    :scale: 50 %
    :align: center

For this dataset, a feature separation of 16 pixels is chosen.

This procedure will probably be automated at some point in the future.

Running the analysis on a HAADF image
-------------------------------------

The next step is running the actual processing using the `make_atom_lattice_from_image`,
using the predefined process parameter `PerovskiteOxide110`, and the same signal `s` as earlier.

The process parameter `PerovskiteOxide110` contain various parameters and names for processing
a perovskite oxide structure projected along the [110] direction.

.. code-block:: python

    >>> process_parameter = am.process_parameters.PerovskiteOxide110()
    >>> atom_lattice = am.make_atom_lattice_from_image(s, process_parameter=process_parameter, pixel_separation=16)

Depending on the size of the dataset, this can take a while. 
For the test dataset used here it should take about 1 minute.

The processing will:
    1. Locate the most intense atomic columns (Strontium).
    2. Refine the position using center of mass.
    3. Refine the position using 2-D Gaussian distributions
    4. Find the translation symmetry using nearest neighbor statistics, and construct atomic planes using this symmetry.
    5. Locate the second most intense atomic columns (Titanium), using the parameters defined in the model parameters
    6. "Subtract" the intensity of the Strontium from the HAADF image
    7. Refine the position of the Titanium using center of mass
    8. Refine the position of the Titanium using 2-D Gaussian distributions
    9. Construct atomic planes in the same way as for the first sublattice.

This returns an `atom_lattice` object, which contains several utility functions.
For example `get_sublattice_atom_list_on_image` returns a HyperSpy signal which shows all the located atomic positions.

.. code-block:: python

    >>> atom_lattice.get_sublattice_atom_list_on_image().plot(plot_markers=True)

.. image:: images/tutorial/atomlattice_plot_atoms.jpg
    :scale: 50 %
    :align: center

Sublattices can be accessed using `atom_lattice.sublattice_list`:

.. code-block:: python

    >>> sublattice = atom_lattice.sublattice_list[0]

These `sublattice` objects contain a large amount of information about
the atomic columns:

.. code-block:: python

    >>> sublattice.x_position
    >>> sublattice.y_position
    >>> sublattice.sigma_x
    >>> sublattice.sigma_y
    >>> sublattice.ellipticity
    >>> sublattice.rotation

These can be saved in different formats such as Numpy npz file:

.. code-block:: python

    >>> import numpy as np
    >>> np.savez("datafile.npz", x=sublattice.x_position, y=sublattice.y_position)

Or comma-separated values (CSV) file, which can be opened in spreadsheet software:

.. code-block:: python

    >>> np.savetxt("datafile.csv", (sublattice.x_position, sublattice.y_position, sublattice.sigma_x, sublattice.sigma_y, sublattice.ellipticity), delimiter=',')

`sublattice` objects also contain a several plotting functions.
Since the image is from a |SrTiO3| single crystal, there should be no variations in the structure.
So any variations are due to factors such as scanning noise, sample drift and possibly bad fitting.

.. code-block:: python

    >>> s_monolayer = sublattice.get_monolayer_distance_map()
    >>> s_monolayer.plot(plot_markers=True)
    >>> s_elli = sublattice.get_ellipticity_map()
    >>> s_elli.plot(plot_markers=True)

These signals can be saved by using the inbuilt `save` function in the signals.

.. code-block:: python

    >>> s_monolayer.save("monolayer_distances.hdf5")

The `sublattice` objects also contain a list of all the atomic planes:

.. code-block:: python

    >>> sublattice.atom_plane_list

The `atom_plane` objects contain the atomic columns belonging to the same specific plane.
Atom plane objects are defined by the direction vector parallel to the atoms in the plane, for example (58.81, -41.99).
These can be accessed by:

.. code-block:: python

    >>> atom_plane = sublattice.atom_plane_list[0]
    >>> atom_plane.atom_list

The atom planes can be plotted by using the `get_all_atom_planes_by_zone_vector` function, where the zone vector is changed by using the left-right arrow keys:

.. code-block:: python

    >>> sublattice.get_all_atom_planes_by_zone_vector().plot(plot_markers=True)

.. image:: images/tutorial/atomic_planes.jpg
    :scale: 50 %
    :align: center

The `atom_position` objects contain information related to a specific atomic column.
For example:

.. code-block:: python

    >>> sublattice.atom_list
    >>> atom_position = sublattice.atom_list[0]
    >>> atom_position.pixel_x
    >>> atom_position.pixel_y
    >>> atom_position.sigma_x
    >>> atom_position.sigma_y
    >>> sublattice.get_atom_list_on_image().plot(plot_markers=True)

Basic information about the `atom_lattice`, `sublattice`, `atom_plane` and `atom_position` objects can be accessed by simply:

.. code-block:: python

    >>> atom_lattice
    <Atom_Lattice, test_ADF_cropped (sublattice(s): 2)>
    >>> sublattice
    <Sublattice, test_ADF_cropped.A (atoms:237,planes:7)>
    >>> atom_plane
    <Atom_Plane, (29.14, -0.18) (atoms:17)>
    >>> atom_position
    <Atom_Position,  (x:26.1,y:404.7,sx:4.4,sy:5.1,r:0.2,e:1.2)>

The `atom_lattice` object with all the atom positions can be saved:

.. code-block:: python

    >>> atom_lattice.save()

This will make a HDF5-file in the current working directory.
The `atom_lattice` object can then be restored using:

.. code-block:: python

    >>> atom_lattice_1 = am.load_atom_lattice_from_hdf5("test_ADF_cropped_atom_lattice.hdf5")

This is especially useful for large datasets, where refining the atomic positions can take a long time.

Finding the oxygen columns
--------------------------

Atomap can also find the positions of oxygen columns in an Annular Bright Field (ABF) image, by firstly using an ADF image.
We use the same ADF image as earlier, in addition to an ABF image acquired simultaneously:

.. code-block:: python

    >>> urllib.request.urlretrieve("https://gitlab.com/atomap/atomap/raw/master/atomap/tests/datasets/test_ADF_cropped.hdf5", "test_ADF_cropped.hdf5")
    >>> s = hs.load("test_ADF_cropped.hdf5")
    >>> urllib.request.urlretrieve("https://gitlab.com/atomap/atomap/raw/master/atomap/tests/datasets/test_ABF_cropped.hdf5", "test_ABF_cropped.hdf5")
    >>> s_abf = hs.load("test_ABF_cropped.hdf5")
    >>> process_parameter = am.process_parameters.PerovskiteOxide110()
    >>> atom_lattice = am.make_atom_lattice_from_image(s, process_parameter=process_parameter, pixel_separation=16, s_image1=s_abf)
    >>> atom_lattice
    <Atom_Lattice, test_ADF_cropped (sublattice(s): 3)>

The oxygen `sublattice` has been added to the `atom_lattice`.
This new `sublattice` can be visualized using `get_sublattice_atom_list_on_image`:

.. code-block:: python

    >>> atom_lattice.get_sublattice_atom_list_on_image().plot(plot_markers=True)

.. image:: images/tutorial/all_sublattice_oxygen.jpg
    :scale: 50 %
    :align: center

.. |SrTiO3| replace:: SrTiO\ :sub:`3`


Jupyter Notebook
----------------

There is also a tutorial in the form of a Jupyter Notebook, which covers similar topics as in this tutorial: https://gitlab.com/atomap/atomap_demos/blob/master/notebook_example/Atomap.ipynb
