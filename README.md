Atomap is a Python library for analysing atomic resolution
scanning transmission electron microscopy images.
It relies on fitting 2-D Gaussian functions to every atomic
column in an image, and automatically finding all the atomic
planes with the largest spacings.

More information can be found on [http://atomap.org](http://atomap.org).
